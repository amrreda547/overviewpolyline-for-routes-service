'use strict';
require('dotenv').config()
let db = require('./dbConnection')
const Route = require('./models/Route')
const axios = require('axios')
const decodePolyline = require('decode-google-map-polyline');


// process.env.NODE_ENV = 'production';

module.exports.updateOverviewPolyline = async () => {

    await db.init()

    let allRoutes = await Route.find({}).populate({
		path: 'stations',
		model: 'Station'
	})
    
    if (!allRoutes) return
    for (let index = 0; index < allRoutes.length; index++) {
        const route = allRoutes[index];
        if (route.stations.length == 0) break
        let firstStation = route.stations[0]
        let lastStation = route.stations[route.stations.length - 1]
        let waypoints = prepareWayPoints(route.waypoints)
        const res = await axios({
            method: 'GET',
            url: 'https://maps.googleapis.com/maps/api/directions/json'
                + '?origin=' + firstStation.location.coordinates[0] + ',' + firstStation.location.coordinates[1]
                + '&destination=' + lastStation.location.coordinates[0] + ',' + lastStation.location.coordinates[1]
                + '&waypoints=' + waypoints
                + '&key=' + process.env.GOOGLE_API_KEY
        })
        
        if (res.data.routes[0]) {
            console.log('ROUTE ID ~~~~ ', route._id);
                    
            let encodedOverviewPolyline = res.data.routes[0].overview_polyline.points
            let decodedOverviewPolyline = decodePolyline(encodedOverviewPolyline)
            await Route.findByIdAndUpdate(route._id, {encodedOverviewPolyline, decodedOverviewPolyline}, { new: true })
        }
    }

    await db.close()
};

const prepareWayPoints = (oldWaypoints) => {
    let newWayPoints = ''
    for (let index = 0; index < oldWaypoints.length; index++) {
        if (index != 0) newWayPoints += '|'
        const oldWayPoint = oldWaypoints[index];
        newWayPoints += oldWayPoint.coordinates[0] + ',' + oldWayPoint.coordinates[1]
    }
    return newWayPoints
}