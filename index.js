require('dotenv').config()
const {updateOverviewPolyline} = require('./handler');
setInterval(async () => {
    try {
        await updateOverviewPolyline()
    } catch (error) {
        console.log(error);
    }
}, process.env.SCHEDULER_TIME_IN_HOURS * 60 * 60 * 1000);

