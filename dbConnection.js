let mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false)

const init = async () => {

	await mongoose.connect(process.env.MONGO_HOST, { useNewUrlParser: true })
	console.log('connection opened')
	let db = mongoose.connection

	db.on('error', async ()=>{
		await mongoose.connection.close()
	})
}

const close = async () => {
	await mongoose.connection.close()
	console.log('connection closed')
}

module.exports = {
	init,
	close
}