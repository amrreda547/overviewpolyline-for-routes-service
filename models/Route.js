const mongoose = require('mongoose')
const pointSchema = require('./pointSchema')
const Station = require('./Station')

const wayPointSchema = new mongoose.Schema({
    lat: {
        type: Number
    },
    lng: {
        type: Number
    }
})

const routeSchema = new mongoose.Schema({
    overviewPolylineId: {
        type: String
    },
    routeName: {
        type: String,
        required: true
    },
    stations: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Station',
        required: true
    },
    waypoints: {
        type: [pointSchema],
        required: true,
        index: '2dsphere'
    },
    encodedOverviewPolyline: {
        type: String,
        default: '',
    },
    decodedOverviewPolyline: {
        type: [wayPointSchema],
        default: [],
    },
    createdAt: {
        type: Date,
        default: Date.now,
        required: true
    },
    updatedAt: {
        type: Date,
        default: Date.now,
        required: true
    }
})

routeSchema.method('getJSON', async function() {
	return {
		id: this._id,
		routeName: this.routeName,
		stations: this.stations,
		waypoints: this.waypoints
	}
})

routeSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})

routeSchema.pre('findOneAndUpdate', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})

const Route = mongoose.model('Route', routeSchema)

module.exports = Route
