const mongoose = require('mongoose')
const pointSchema = require('./pointSchema')

const stationSchema = new mongoose.Schema({
	nameEn: {
		type: String,
		required: true
	},
	nameAr: {
		type: String,
		required: true
	},
	location: {
		type: pointSchema,
		required: true,
		index: '2dsphere'
	},
	route: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Route'
	},
	createdAt: {
		type: Date,
		default: Date.now(),
		required: true
	},
	updatedAt: {
		type: Date,
		default: Date.now(),
		required: true
	}
})

stationSchema.method('getJSON', async function() {
	return {
		id: this._id,
		nameEn: this.nameEn,
		nameAr: this.nameAr,
		location: this.location,
		route: this.route
	}
})

stationSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})

stationSchema.pre('findOneAndUpdate', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})

const Station = mongoose.model('Station', stationSchema)

module.exports = Station
