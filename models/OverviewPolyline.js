const mongoose = require('mongoose')
const overviewPolylineSchema = new mongoose.Schema({
	points: {
		type: String,
		required: true
	},
	createdAt: {
		type: Date,
		default: Date.now(),
		required: true
	},
	updatedAt: {
		type: Date,
		default: Date.now(),
		required: true
	}
})

overviewPolylineSchema.method('getJSON', async function() {
	return {
		id: this._id,
		points: this.points
	}
})

overviewPolylineSchema.pre('update', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})

overviewPolylineSchema.pre('findOneAndUpdate', function(next) {
	this.update({}, { $set: { updatedAt: new Date() } })
	next()
})
const OverviewPolyline = mongoose.model('OverviewPolyline', overviewPolylineSchema)

module.exports = OverviewPolyline
